import notificationApi from "../apis/notificationApi";
import { sendNotificationToClient } from "./../services/notify";

const NotificationController = {
  sendNotification: async (req, res) => {
    // const { title, body, type, user_id } = req.body;
    // const notification = { title, body };
    const notification = {
      title: "Trung Hau",
      body: "Xin chao cac ban minh la Trung Hau",
    };

    let tokens = [
      "fkzpibz5EUujDi7tDYE02X:APA91bHKMcUXjuYiwFcNamq1gCxs2FMD19w-esXllye21k8Q5uLQaVyNYrR_MFLp5ZFik8cySjcYNl0IvVAKtSkTwXpEU01nW3h9Ife6ioktF4vsKrVi3nwJH-PPkwlrWT4MscREChHh",
    ];

    // if (type === "ONE_USER") {
    //   try {
    //     const resp = await notificationApi.getToken({ user_id });
    //     if (resp.status === "SUCCESS") {
    //       tokens = resp.data.map((token) => token.token);
    //     }
    //   } catch (err) {
    //     console.log(err);
    //   }
    // }

    // if (type === "ALL_USER") {
    //   try {
    //     const resp = await notificationApi.getAllToken();
    //     if (resp.status === "SUCCESS") {
    //       tokens = resp.data.map((token) => token.token);
    //     }
    //   } catch (err) {
    //     console.log(err);
    //   }
    // }

    sendNotificationToClient(tokens, notification);
    return res.json({ status: "SUCCESS" });
  },
};

export default NotificationController;
