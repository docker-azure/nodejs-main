FROM node:14-alpine

WORKDIR /app

RUN npm install -g pm2

COPY ["package.json", "yarn.lock", "./"]

RUN npm install --production --silent

COPY . .

CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]